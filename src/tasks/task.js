import React, { Component } from 'react';
import './taskItem.css';

class TaskItem extends Component {
    constructor(props) {
        super(props);
    }

    removeTask(id) {
        this.props.removeTask(id);
    }

    render() {
        return(
            <div className="taskWrapper">
                <button className="removeTask" onClick={(e) => this.removeTask(e, this.props.id)}> remove </button>{this.props.task.text}
            </div>
        )
    }


}

export default TaskItem;
