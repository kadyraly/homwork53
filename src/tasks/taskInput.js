import React from 'react';
import './taskInput.css';

export default class TaskInput extends React.Component {
    constructor(props) {
        super(props)

        this.state = {value: ""};

        this.handleChange = this.handleChange.bind(this);
        this.addTask = this.addTask.bind(this);
    }

    handleChange (e) {
        this.setState({value: e.target.value});
    }

    addTask (task) {
        if(task.length > 0) {
            this.props.addTask(task);
            this.setState({value: ""});
        }
    }

    render() {
       return (
           <div>
               <input type="text" value={this.state.value} onChange={this.handleChange} />
               <button className="btn btn-primary" onClick={() => this.addTask(this.state.value)}> Add </button>
           </div>
       )
    }
}