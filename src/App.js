import React, { Component } from 'react';
import './App.css';
import Header from './tasks/header';
import TaskInput from './tasks/taskInput';
import TaskItem from  './tasks/task';

class App extends Component {
    constructor(props) {
        super(props);

        this.state = {
            tasks: [
                {id: 0, text: "Make dinner"},
                {id: 1, text: "Read book"},
                {id: 2, text: "Learn English"}
            ],
            nextId: 3
        }

        this.addTask = this.addTask.bind(this);
        this.removeTask = this.removeTask.bind(this);
    }

    addTask(taskText) {
        let tasks = this.state.tasks.slice();
        tasks.push({id: this.state.nextId, text: taskText});
        this.setState({
            tasks: tasks,
            nextId: ++this.state.nextId
        });
    }

    removeTask(id) {
        this.setState({
            tasks: this.state.tasks.filter((task, index) => task.id !== id)
        })
    }

    render() {
        return (
            <div className="App">
                <div className="task-wrapper">
                    <Header />
                    <TaskInput taskText="" addTask={this.addTask}/>
                    <ul>
                        {
                            this.state.tasks.map((task) => {
                            return <TaskItem task={task} key={task.id} id={task.id} removeTask={() => this.removeTask(task.id)}/>
                        })
                        }
                    </ul>
                </div>
            </div>
        )
    }
}


export default App;